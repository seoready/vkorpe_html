***README***
[vkorpe.seoready.ru](http://vkorpe.seoready.ru)



1. Для работы якорей с плавным переходом, необходимо повесить класс ```.anchor``` на ссылку:

```html
	// якоря
    <a href="#someid" class="anchor">переход на...</a>
```


2. Можно добавить кнопку ```подняться наверх```, используя кнопку с классом ```.upbutton```:

```html
	<a href="#" class="upbutton">наверх</a>
```

3. Переход со слайдера проектов настраивается через атрибут ```data-href='/'``` тега ```.projects__slider_item```.

4. форма ```перезвоните нам``` вызывается по классу ```.call__back_btn``` на ссылке с атрибутом ```href='javascript://'```.

```.request__btn``` - отвечает за открытие формы для обратной связи:

```html
	<a href="javascript://" class="call__back_btn btn__white btn">Перезвоните нам</a>
	<a href="javascript://" class="request__btn btn__white btn">Задать вопрос</a>
```

5. Для установки видео в слайдер на гл. стр., необходимо учесть как минимум три формата видео в теге ```<video>``` (mp4,webm,ogv):

```html
	<video class="slider_background_video" loop="loop" autoplay="autoplay" muted="muted" poster="https://html5backgroundvideos.com/wp-content/uploads/2015/09/sketching-mock-up.jpg" id="bgvid">
		<source src="https://d2ezlykacdqcnj.cloudfront.net/sketching-mock-up/sketching-mock-up.mp4" type="video/mp4">
		<source src="https://d2ezlykacdqcnj.cloudfront.net/sketching-mock-up/sketching-mock-up.webm" type="video/webm">
		<source src="https://d2ezlykacdqcnj.cloudfront.net/sketching-mock-up/sketching-mock-up.ogv" type="video/ogg">
	</video>
```

6. Разновидности кнопок:

```html
	<a href="#" class="btn">Прозрачная кнопка с белым текстом и белой рамкой</a>
	<a href="#" class="btn btn__dark">Прозрачная кнопка с черным текстом и черной рамкой</a>
	<a href="#" class="btn btn__blue">Фирменная градиентная кнопка</a>
	<a href="#" class="btn btn__small">Фирменная маленькая цветная</a>
	<a href="#" class="btn btn__small btn__small_active">Фирменная маленькая цветная активная</a>
```

7. На странице ```contacts.html``` внизу выведена секция ```<script></script>``` в которой находится код:

```js
// настройки гугл карты
    mapContainer = "map"; //id
    zooming = 16; //МАСШТАБ
    markerImg = 'img/mapmarker.svg';

    // Места магазинов
    posOn1 = { lat: 59.873677, lng: 30.337133 }; //СПБ
    posOn2 = { lat: 55.714481, lng: 37.6432014 }; //МСК

    // Контент маркеров
    markerContent1 = "<div class='mapMarker'><h5>Санкт-Петербург</h5><br><a href='tel:88122442717'>8 (812) 244 27 17</a><br>проспект Юрия Гагарина, дом 2, 4 этаж<br><a href='mailto:zakaz@vkorpe.ru'>zakaz@vkorpe.ru</a></div>";

    markerContent2 = "<div class='mapMarker'><h5>Москва</h5><br><a href='tel:84997033035'>8 (499) 703 30 35</a><br>Павелецкая набережная, дом 2, строение 3<br><a href='mailto:zakaz@vkorpe.ru'>zakaz@vkorpe.ru</a></div>";

    // Тайтлы маркеров
    markerTitle1 = "Вкорпе Санкт-Петербург"; //title при наведении на маркер
    markerTitle2 = "Вкорпе Москва"; //title при наведении на маркер
```
В коде может использоваться две точки на карте.

В теге:
```html
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=[YOURAPIKEY]"></script>
```
вместо "[YOURAPIKEY]" необходимо вставить апи ключ гугл карт.

**Получение API ключа google maps**

```
https://developers.google.com/maps/documentation/maps-static/get-api-key
```

**Компиляция ресурсов**

Для компиляции ресурсов(js,css) и оптимальной их работы на постпродакшене используется GUI препроцессор [CodeKit 3.9.2](https://codekitapp.com/):

```
Исходник js: /js/main.js
Исходник scss: /css/main.scss
```



