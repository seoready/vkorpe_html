// @codekit-prepend "1_jquery-3.3.1.min.js", "2_parlx_pacejs_jqeasing.js", "3_jquery.arcticmodal-0.3.min.js", "4_slick.min.js", "5_jquery.spincrement.min.js", "6_accordion.js", "7_masonry.pkgd.min.js", "8_isotope.pkgd.js", "9_mixitup.min.js";
Pace.on('done', function() {

    // скрываем меню при скроле вниз
    if ($('.navigation').length) {
        var menuoffsettop = $(".navigation").offset().top;
        $(window).scroll(function() {
            if ($(window).scrollTop() > menuoffsettop) {
                $(".navigation").addClass('nav_hidehook');

            } else {
                $(".navigation").removeClass('nav_hidehook');

            }
        });
    }
    
    if ($('.wrapper_humans').length) {
        $('.wrapper_slide').click(function(event) {
            $('.wrapper_slide').toggleClass('wrapper_slide__rotate');
            $('.wrapper_humans__main').toggleClass('wrapper_humans', 1000);
        });
    }




    /////////////start_scroll///////////////


    // function SmoothScroll(target, speed, smooth) {
    //     if (target === document)
    //         target = (document.scrollingElement ||
    //             document.documentElement ||
    //             document.body.parentNode ||
    //             document.body) // cross browser support for document scrolling

    //     var moving = false
    //     var pos = target.scrollTop
    //     var frame = target === document.body &&
    //         document.documentElement ?
    //         document.documentElement :
    //         target // safari is the new IE

    //     target.addEventListener('mousewheel', scrolled, { passive: false })
    //     target.addEventListener('DOMMouseScroll', scrolled, { passive: false })

    //     function scrolled(e) {
    //         e.preventDefault(); // disable default scrolling

    //         var delta = normalizeWheelDelta(e)

    //         pos += -delta * speed
    //         pos = Math.max(0, Math.min(pos, target.scrollHeight - frame.clientHeight)) // limit scrolling

    //         if (!moving) update()
    //     }

    //     function normalizeWheelDelta(e) {
    //         if (e.detail) {
    //             if (e.wheelDelta)
    //                 return e.wheelDelta / e.detail / 40 * (e.detail > 0 ? 1 : -1) // Opera
    //             else
    //                 return -e.detail / 3 // Firefox
    //         } else
    //             return e.wheelDelta / 120 // IE,Safari,Chrome
    //     }

    //     function update() {
    //         moving = true

    //         var delta = (pos - target.scrollTop) / smooth

    //         target.scrollTop += delta

    //         if (Math.abs(delta) > 0.5)
    //             requestFrame(update)
    //         else
    //             moving = false
    //     }

    //     var requestFrame = function() { // requestAnimationFrame cross browser
    //         return (
    //             window.requestAnimationFrame ||
    //             window.webkitRequestAnimationFrame ||
    //             window.mozRequestAnimationFrame ||
    //             window.oRequestAnimationFrame ||
    //             window.msRequestAnimationFrame ||
    //             function(func) {
    //                 window.setTimeout(func, 1000 / 50);
    //             }
    //         );
    //     }()
    // }

    // // SmoothScroll(target,speed,smooth)
    // SmoothScroll(document, 80, 8);
    // /////////////end_scroll///////////////




    // якоря
    $('.anchor, .going').on('click', function(event) {
        var link = $(this).attr('href');
        jQuery("html:not(:animated),body:not(:animated)").animate({ scrollTop: $(link).offset().top }, 2000, 'easeInOutExpo');
        return false;
    });



    $('.projects__slider_item').each(function(index, el) {
        console.log();
        $(this).click(function(event) {
            /* Act on the event */
            location.href = $(this).data('href');
        });
    });

    // показ при скроле подняться наверх
    // $(window).scroll(function() {
    //     if ($(this).scrollTop() > 300) {
    //         $('.upbutton').fadeIn();
    //     } else {
    //         $('.upbutton').fadeOut();
    //     }
    // });

    // подняться наверх
    $('.upbutton').click(function() {
        $("html:not(:animated),body:not(:animated)").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
    });



    // Project Slider
    $('.projects__slider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        slidesToShow: 2,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
                arrows: false
            }
        }]
    });

    if ($('.solutions__items_block').length) {

        var show = true;
        var countbox = ".solutions__items_block";
        $(window).on("scroll load resize", function() {
            if (!show) return false;
            var w_top = $(window).scrollTop();
            var e_top = $(countbox).offset().top;
            var w_height = $(window).height();
            var d_height = $(document).height();
            var e_height = $(countbox).outerHeight();
            if (w_top + 600 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
                $('.solutions__items_block').css('opacity', '1', 'animation-delay', '2');
                $('.solutions__items_block_number').spincrement({
                    thousandSeparator: "",
                    duration: 5000
                });

                show = false;
            }
        });
    }

    $('.equipment__links').slick({
        dots: false,
        infinite: false,
        autoplay: false,
        slidesToShow: 7,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    arrows: false
                }
            },

            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
        ]
    });


    $('.parallax').parlx({
        settings: {
            // options...
            height: "auto",
            type: "foreground",
        },
        callbacks: {
            // callbacks...
        }
    });


    // Fixed navigation

    $(window).scroll(function() {
        if ($(this).scrollTop() > 80) {
            $('.navigation').addClass("sticky");
            $('.burger_menu').addClass("sticky");
        } else {
            $('.navigation').removeClass("sticky");
            $('.burger_menu').removeClass("sticky");
        }
    });

    // Main Slider
    $('.mainSlider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        slidesToShow: 1,
        appendDots: '.mainSlider',
        arrows: false
    });

    // Форма заявки

    $('.request__btn').click(function(e) {
        e.preventDefault();
        $('#exampleModal').arcticmodal();
    });

    // Форма обратного звонка

    $('.call__back_btn').click(function(e) {
        e.preventDefault();
        $('#exampleModal2').arcticmodal();
    });

    // Solutions Slider

    $('.solutions_slider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        mobileFirst: true
    });

    // Burger

    $('.burger').click(function() {
        if ($('.burger_menu').hasClass('hidden')) {
            $('.burger_menu').removeClass('hidden')
        } else {
            $('.burger_menu').addClass('hidden')
        }
    })

    // $(".burger_menu__item").accordionjs({
    //     activeIndex: false,
    //     closeAble: true,
    //     closeOther: true
    // });


    $('.burger_menu__list > .burger_menu__item > a, .burger_menu__item_acc > a').click(function(e) {
        e.preventDefault();
    })

    detectWidth = function(winWidth) {
        if ($(window).width() < winWidth) {
            $('.burger_menu__list').accordionjs({
                activeIndex: false,
                closeAble: true,
                closeOther: true
            });
            $('.burger_menu_other').accordionjs({
                activeIndex: false,
                closeAble: true,
                closeOther: true
            });

        } else {
            return false;
        };

    }

    detectWidth(1200);
    $(window).resize(function() {
        detectWidth(1200);
    });

    // Анимация

    $(window).scroll(function() {
        $('.animated').each(function() {
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 900) {
                $(this).addClass("fadeIn");
            }
        });


        // $('animated').each(function() {
        //     var imagePos = $(this).offset().top;

        //     var topOfWindow = $(window).scrollTop();
        //     if (imagePos < topOfWindow + 750) {
        //         $(this).addClass("fadeInLeft");
        //     }
        // });

        // $('.animated').each(function() {
        //     var imagePos = $(this).offset().top;

        //     var topOfWindow = $(window).scrollTop();
        //     if (imagePos < topOfWindow + 750) {
        //         $(this).addClass("fadeInRight");
        //     }
        // });
    });


    if ($('#mix_container').length) {


        var containerEl = document.querySelector('#mix_container');
        var checkboxGroup = document.querySelector('.checkbox-group');
        var checkboxes = checkboxGroup.querySelectorAll('input[type="checkbox"]');

        var mixer = mixitup(containerEl);

        checkboxGroup.addEventListener('change', function() {
            var selectors = [];

            for (var i = 0; i < checkboxes.length; i++) {
                var checkbox = checkboxes[i];

                if (checkbox.checked) selectors.push(checkbox.value);
            }

            var selectorString = selectors.length > 0 ?
                selectors.join(',') :
                'all';

            mixer.filter(selectorString);
        });
    }

    $('.map').append('<div class="mapButtons"><div class="spbButton btn btn__small btn__small_active">Санкт-Петербург</div><div class="mskButton btn btn__small">Москва</div></div>');

    $('.spbButton').click(function(event) {
        /* Act on the event */
        $('.mskButton').removeClass('btn__small_active');
        $(this).addClass('btn__small_active');
        map.setCenter(posOn1);
    });

    $('.mskButton').click(function(event) {
        /* Act on the event */
        $('.spbButton').removeClass('btn__small_active');
        $(this).addClass('btn__small_active');

        map.setCenter(posOn2);
    });




});


// googleMap
if ($('#map').length) {
    mapInit = function() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: zooming,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(posOn1),

            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#616161"
                    }]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#bdbdbd"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#eeeeee"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#757575"
                    }]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "labels.icon",
                    "stylers": [{
                            "color": "#b7b7b7"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "labels.text",
                    "stylers": [{
                            "color": "#b4b4b4"
                        },
                        {
                            "visibility": "simplified"
                        },
                        {
                            "weight": 0.5
                        }
                    ]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "color": "#b3b8b6"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e5e5e5"
                    }]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#757575"
                    }]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dadada"
                    }]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#616161"
                    }]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e5e5e5"
                    }]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#eeeeee"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#c9c9c9"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                }
            ],
            // ui для изменения масштабирования
            gestureHandling: 'cooperative',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },

        };

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        mapElement = document.getElementById(mapContainer);

        // Create the Google Map using our element and options defined above
        map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker1 = new google.maps.Marker({
            position: new google.maps.LatLng(posOn1),
            map: map,
            // тайтл для маркера при наведении
            title: markerTitle1,

            // Иконка для маркера
            icon: markerImg,

            //анимация маркера
            // animation: google.maps.Animation.BOUNCE


        });
        var marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(posOn2),
            map: map,
            // тайтл для маркера при наведении
            title: markerTitle2,

            // Иконка для маркера
            icon: markerImg,

            //анимация маркера
            // animation: google.maps.Animation.BOUNCE


        });

        // создаем переменную с параметрами первого маркера
        // отключаем анимацию
        marker1.addListener('click', function() {
            // marker.setAnimation(null);
        });
        marker2.addListener('click', function() {
            // marker.setAnimation(null);
        });

        // создаем переменную info для информационного окна InfoWindow
        var info1 = new google.maps.InfoWindow({

            // контент для инфо окна
            content: markerContent1

        });
        var info2 = new google.maps.InfoWindow({

            // контент для инфо окна
            content: markerContent2

        });

        // Добавляем отслеживание события клика для маркера marker1
        marker1.addListener('click', function() {

            // Вызываем открытие инфоокна с параметрами map (применимость к карте), marker1(над каким маркером открывать инфоокно)
            info1.open(map, marker1)
        });

        marker2.addListener('click', function() {

            // Вызываем открытие инфоокна с параметрами map (применимость к карте), marker1(над каким маркером открывать инфоокно)
            info2.open(map, marker2)
        });
    }



    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', mapInit);

    // end_googleMap

} else {

}